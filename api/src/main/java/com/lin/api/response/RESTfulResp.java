package com.lin.api.response;

import com.lin.api.response.sugar.LanguageSugar;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RESTfulResp implements LanguageSugar<RESTfulResp> {
    private int code;
    private String msg;
    private Object data;
    private Date timestamp;
    private String error;

    public RESTfulResp(int code, String msg, String error, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.error = error;
        this.timestamp = new Date();
    }


    public RESTfulResp(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static RESTfulResp ok(Object data) {
        return new RESTfulResp(200, "ok", null, data);
    }

    public static RESTfulResp ok(String msg, Object data) {
        return new RESTfulResp(200, msg, null, data);
    }

    public static RESTfulResp ok() {
        return new RESTfulResp(200,"ok");
    }
    public static RESTfulResp ok(String msg) {
        return new RESTfulResp(200,msg);
    }

    public static RESTfulResp notFound(String msg, String error, Object data) {
        return new RESTfulResp(404, msg, error, data);
    }

    public static RESTfulResp notFound(String msg, Object data) {
        return RESTfulResp.notFound(msg, "Not Found", data);
    }

    public static RESTfulResp notFound(String msg) {
        return RESTfulResp.notFound(msg, null);
    }

    /**
     * 请求异常时返回
     */
    public static RESTfulResp internalServerError(String msg, String error, Object data) {
        return new RESTfulResp(500, msg, error, data);
    }

    public static RESTfulResp internalServerError(String msg, Object data) {
        return RESTfulResp.internalServerError(msg, "Internal Server Error", data);
    }

    public static RESTfulResp internalServerError(String msg) {
        return new RESTfulResp(500, msg);
    }

    public static RESTfulResp badRequest(String msg, String error, Object data) {
        return new RESTfulResp(400, msg, error, data);
    }


    public static RESTfulResp badRequest(String msg, Object data) {
        return RESTfulResp.internalServerError(msg, "Bad Request", data);
    }

    public static RESTfulResp badRequest(String msg) {
        return new RESTfulResp(400,msg);
    }

    @Override
    public RESTfulResp returnThis() {
        return this;
    }

    public static RESTfulResp forbidden(String msg) {
        return RESTfulResp.forbidden(msg, null);
    }

    public static RESTfulResp forbidden(String msg, Object data) {
        return RESTfulResp.forbidden(msg, "forbidden", data);
    }

    public static RESTfulResp forbidden(String msg, String error, Object data) {
        return new RESTfulResp(403, msg, error, data);
    }

}
