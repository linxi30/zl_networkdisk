package com.lin.api.response.sugar;



import org.jetbrains.annotations.NotNull;
import java.util.function.Consumer;

/**
 * 这是一个语法糖接口，类实现该接口的{@link #returnThis()}后,
 * 可以使用接口中的方法简化代码编写。
 * <p>
 * 参考kotlin语言的scope function
 */
public interface LanguageSugar<T> {

    T returnThis();

    /**
     * 该方法将对象传入方法，处理完成后返回传入的对象
     */
    default T apply(Consumer<T> it) {
        it.accept(returnThis());
        return returnThis();
    }

    /**
     * 该方法返回操作后的值而不是原对象，由于Java语言限制
     * 目前仅支持::操作符
     */
    default <U> U let(@NotNull Let<T, U> it) {
        return it.accept(returnThis());
    }


}
