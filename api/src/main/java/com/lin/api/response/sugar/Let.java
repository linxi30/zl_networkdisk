package com.lin.api.response.sugar;

public interface Let<T, U> {
    U accept(T it);
}
