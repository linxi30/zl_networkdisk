package com.lin.api.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PageUtil implements Serializable {

/*    root: "data.page.list",
    page: "data.page.currPage",
    total: "data.page.totalPage",
    records: "data.page.totalCount"*/

    //总记录数
    private long totalCount;

    //每页显示纪录数
    private long pageSize;

    //总页数
    private long totalPage;

    //当前页数
    private long currPage;

    //数据列表
    private List<?> list;

    public PageUtil(int totalCount, long pageSize, int totalPage, long currPage, List<?> list) {
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.currPage = currPage;
        this.list = list;
    }

    public PageUtil(IPage<?> page) {
        this.totalCount = page.getTotal();
        this.pageSize = page.getSize();
        this.totalPage = page.getPages();
        this.currPage = page.getCurrent();
        this.list = page.getRecords();
    }
}
