package com.lin.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.model.entity.SysMenuEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {

    //获取所有菜单
    List<SysMenuEntity> queryList();
    //获取不包含按钮的菜单列表
    List<SysMenuEntity> queryNotButtonList();
    //根据父级菜单id查询其下的子菜单列表
    List<SysMenuEntity> queryListParentId(Integer menuId);
}
