package com.lin.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.model.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    //查询用户的所有权限
    List<Integer> queryAllMenuId(Integer userId);

    SysUserEntity selectByUserName(@Param("userName") String userName);

    boolean updatePassword(int userId, String newPsd);
    //查询用户的所有权限
    List<String> queryAllPerms(int userId);


}
