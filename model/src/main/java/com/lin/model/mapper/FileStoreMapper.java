package com.lin.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.model.entity.FileStoreEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileStoreMapper extends BaseMapper<FileStoreEntity> {
}
