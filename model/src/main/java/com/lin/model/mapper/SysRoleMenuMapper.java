package com.lin.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.model.entity.SysRoleMenuEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {


    //根据角色Id列表，批量删除
    void deleteBatch(@Param("roleIds") String roleIds);

    //根据角色id，获取菜单id列表
    List<Integer> queryMenuIdList(Integer roleId);
}
