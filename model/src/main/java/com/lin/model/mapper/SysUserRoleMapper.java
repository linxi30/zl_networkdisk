package com.lin.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.model.entity.SysUserRoleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {
    //根据用户id 获取用户角色Id列表
    List<Integer> queryRoleIdList(Integer userId);

    int deleteBatch(@Param("roleIds") String roleIds);
}
