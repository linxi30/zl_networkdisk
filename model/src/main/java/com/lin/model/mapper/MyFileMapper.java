package com.lin.model.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.model.entity.MyFileEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MyFileMapper extends BaseMapper<MyFileEntity> {


}
