package com.lin.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@TableName("sys_user")
@AllArgsConstructor
@NoArgsConstructor
public class SysUserEntity implements Serializable {

    @TableId
    private int userId;

    private int fileStoreId;

    @NotBlank(message="姓名不能为空!")
    private String name;

    @NotBlank(message="用户名不能为空!")
    private String username;

    @NotBlank(message="密码不能为空!")
    private String password;

    private String salt;

    @NotBlank(message="邮箱不能为空!")
    private String email;

    private Integer status;

    //@TableField：字段属性不为数据库表字段，但又是必须使用的

    @Size(min = 1,message="必须选择一位角色!")
    @TableField(exist=false)
    private List<Integer> roleIdList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;


}
