package com.lin.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("file_store")
public class FileStoreEntity implements Serializable {

    private int fileStoreId;

    private int userId;

    private int currentSize;

    private int maxSize;

    private int permission;
}
