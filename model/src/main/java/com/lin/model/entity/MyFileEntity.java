package com.lin.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@TableName("my_file")
public class MyFileEntity {

    @TableId
    private Integer myFileId;

    @NotBlank(message = "文件名称不能为空")
    private String myFileName;

    private Integer fileStoreId;

    private String myFilePath;

    private Long size;

    private Integer type;

    @NotBlank(message = "文件后缀不能为空")
    private String postfix;

    private Integer isRecovery;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date uploadTime;
}
