package com.lin.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("file_folder")
public class FileFolderEntity {

    private Integer filefolderId;

    private String filefolderName;

    private Integer parentFolderId;

    private Integer fileStoreId;

    private Date time;
}
