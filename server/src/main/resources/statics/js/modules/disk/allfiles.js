$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'file/my/list',
        datatype: "json",
        colModel: [
            { label: '文件名称', name: 'myFileName', index: "my_file_name",align:"center", width: 75, key: true },
            { label: '文件路径', name: 'myFilePath', index: "my_file_path", align:"center", width: 75 },
            { label: '文件大小', name: 'size',index: "size", align:"center", width: 75 },
            { label: '文件后缀', name: 'postfix',index: "postfix", align:"center", width: 75 },
            {label: '类型', name: 'type', align: 'center', valign: 'middle', sortable: true, width: 75, formatter: function(value, options, row){
                    if(value === 1){
                        return '<span class="label label-primary">文档</span>';
                    }
                    if(value === 2){
                        return '<span class="label label-success">图片</span>';
                    }
                    if(value === 3){
                        return '<span class="label label-warning">音乐</span>';
                    }
                    if(value === 4){
                        return '<span class="label label-info">视频</span>';
                    }
                    if(value === 5){
                        return '<span class="label label-danger">压缩包</span>';
                    }
                }},
            { label: '文件上传时间', name: 'uploadTime',index: "upload_time", width: 100 },
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList : [5,10,20,50,100],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.page.list",
            page: "data.page.currPage",
            total: "data.page.totalPage",
            records: "data.page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
});

var vm = new Vue({
    el:'#pmpapp',
    data:{
        q:{
            myFileName: null
        },
        showList: true,
        title:null,

        myfile:{
            myFileId:null,
            myFileName:null,
        }
    },
    methods: {
        query: function () {
            vm.reload();
        },

        reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam','page');
            $("#jqGrid").jqGrid('setGridParam',{
                myfileData:{'search': vm.q.myFileName},
                page:page
            }).trigger("reloadGrid");
        },



    }
});