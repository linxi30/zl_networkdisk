package com.lin.server;

import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@MapperScan("com.lin.model.mapper")
@EnableScheduling
@Slf4j
public class NetWorkDiskApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(NetWorkDiskApplication.class);
    }

    public static void main(String[] args) {
        var run = SpringApplication.run(NetWorkDiskApplication.class,args);
    }
}
