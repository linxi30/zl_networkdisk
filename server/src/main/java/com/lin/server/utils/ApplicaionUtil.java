package com.lin.server.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
@ConfigurationProperties(prefix = "ftp")
public class ApplicaionUtil {

    public static String[] files;

    public void setFiles(String[] files){
        this.files = files;
    }
    //@Scheduled(cron = "10 18 15 * * ?")
    public void test(){
        List<String> list = Arrays.asList(files);
        for (String s : list) {
            System.out.println(s);
        }
    }

}
