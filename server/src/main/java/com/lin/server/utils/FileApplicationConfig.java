package com.lin.server.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
@ConfigurationProperties(prefix = "local")
public class FileApplicationConfig {

    public static String[] files;

    public void setFiles(String[] files){
        this.files = files;
    }
    //@Scheduled(cron = "10 50 14 * * ?")
    public void test(){
        List<String> list = Arrays.asList(files);
        for (String s : list) {
            System.out.println(s);
        }
    }

}
