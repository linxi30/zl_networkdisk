package com.lin.server.utils;


import com.lin.model.entity.MyFileEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@Slf4j
@Component
public class FileUtil {

    private static String localPath;

    @Value("${local.local-path}")
    public void setRootDirectory(String localPath) {
        FileUtil.localPath = localPath;
    }

    /**
     * 创建用户时创建目录
     *
     * @param dirname user_store_id
     */
    public static void createDir(String dirname) {
        File file = null;
        try {
            file = new File(localPath + "\\" + dirname);
            if (!file.exists())
                file.mkdir();
            //目录编码，解决中文路径问题
            String dn = new String(dirname.toString().getBytes("GBK"), "iso-8859-1");
            //获取文件目录名
            List<String> list = Arrays.asList(FileApplicationConfig.files);
            //创建用户文件目录
            for (String dir : list) {
                File file2 = new File(file, dir);
                if (!file2.exists()) {
                    if (file2.mkdir()) {
                        log.info("创建用户根目录成功：{}", dir);
                    }
                }
            }
        } catch (Exception e) {
            log.info("创建用户根目录失败{}:", dirname);
            e.printStackTrace();
        }
    }

    /**
     * 指定用户的文件上传
     */
    public static boolean fileupload(MultipartFile file, MyFileEntity entity) {
        boolean flag = false;
        //检查目录是否存在，正确
        File f1 = new File(localPath, String.valueOf(entity.getFileStoreId()));
        //获取文件后缀
        if (f1.exists()){
            //获取文件类型
            String filetype = transformation(entity.getType());
            //进入文件类型目录
            File f2 = new File(f1, filetype);
            if (f2.exists()){
                //开始上传文件
                try {
                    file.transferTo(new File(f2,entity.getMyFileName()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 文件类型转换为文件
     */
    public static String transformation(Integer type) {
        String result = null;
        if (type == null)
            return result;
        //获取文件目录名
        List<String> list = Arrays.asList(ApplicaionUtil.files);
        if (type == 1) {
            result = list.get(1);
        } else if (type == 2) {
            result = list.get(2);
        } else if (type == 3) {
            result = list.get(3);
        } else if (type == 4) {
            result = list.get(4);
        } else if (type == 5) {
            result = list.get(5);
        }
        return result;
    }
}
