package com.lin.server.utils;


import com.lin.model.entity.MyFileEntity;
import org.apache.commons.net.ftp.FTP;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;


@Slf4j
@Component
public class FtpUtil {

    private static String ftpPost;
    private static String ftpUsername;
    private static String ftpPassword;
    private static Integer ftpPort;
    private static String ftpPath;

    @Value("${ftp.post}")
    public void setFtpPost(String ftpPost) {
        FtpUtil.ftpPost = ftpPost;
    }

    @Value("${ftp.username}")
    public void setFtpUsername(String ftpUsername) {
        FtpUtil.ftpUsername = ftpUsername;
    }

    @Value("${ftp.password}")
    public void setFtpPassword(String ftpPassword) {
        FtpUtil.ftpPassword = ftpPassword;
    }

    @Value("${ftp.port}")
    public void setFtpPort(String ftpPort) {
        FtpUtil.ftpPort = Integer.parseInt(ftpPort);
    }

    @Value("${ftp.ftp-path}")
    public void setFtpPath(String ftpPath) {
        FtpUtil.ftpPath = ftpPath;
    }

    /**
     * 连接ftp服务器1
     */
    public static FTPClient getFtpClient2() {
        FTPClient ftpClient = null;
        try {
            //创建一个ftp客户端
            ftpClient = new FTPClient();
            //设置传输命令的超时
            ftpClient.setDefaultTimeout(60000);
            //设置两个服务器连接超时时间
            ftpClient.setConnectTimeout(200000);
/*            //被动模式下设置数据传输的超时时间
            ftpClient.setDataTimeout(15000);*/
            //被动模式
            ftpClient.enterLocalPassiveMode();
            //连接ftp服务器
            if (!ftpClient.isConnected()){
                ftpClient.connect(ftpPost, ftpPort);
            }
            //登录ftp服务器
            ftpClient.login(ftpUsername, ftpPassword);
            //设置编码
            ftpClient.setControlEncoding("UTF-8");
            //设置传输文件类型
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                log.info("未连接到服务器,账号或者密码错误：{}", ftpPost);
                //关闭连接
                ftpClient.disconnect();
            } else {
                log.info("连接服务器成功：{}", ftpPost);
            }
        } catch (Exception e) {
            log.info("连接ftp服务器失败：{}", ftpPost);
            e.printStackTrace();
        }
        return ftpClient;
    }
    /**
     * 连接ftp服务器
     */
    public static FTPClient getFtpClient() {
        FTPClient ftpClient = null;
        try {
            //创建一个ftp客户端
            ftpClient = new FTPClient();
            //被动模式
            ftpClient.enterLocalPassiveMode();
            //连接ftp服务器
            if (!ftpClient.isConnected()){
                ftpClient.connect(ftpPost, ftpPort);
            }
            //登录ftp服务器
            ftpClient.login(ftpUsername, ftpPassword);
            //设置编码
            ftpClient.setControlEncoding("UTF-8");
            //设置传输文件类型
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                log.info("未连接到服务器,账号或者密码错误：{}", ftpPost);
                //关闭连接
                ftpClient.disconnect();
            } else {
                log.info("连接服务器成功：{}", ftpPost);
            }
        } catch (Exception e) {
            log.info("连接ftp服务器失败：{}", ftpPost);
            e.printStackTrace();
        }
        return ftpClient;
    }

    /**
     * 创建用户时创建目录
     *
     * @param dirname user_store_id
     */
    public static void createDir(String dirname) {
        FTPClient ftpClient = null;
        try {
            //连接ftp
            ftpClient = getFtpClient();
            //目录编码，解决中文路径问题
            String dn = new String(dirname.toString().getBytes("GBK"), "iso-8859-1");
            //创建用户目录
            ftpClient.makeDirectory(dirname);
            //获取文件目录名
            List<String> list = Arrays.asList(ApplicaionUtil.files);
            //创建用户文件目录
            for (String dir : list) {
                //尝试切入目录
                if (ftpClient.changeWorkingDirectory(dn))
                    continue;
                if (ftpClient.makeDirectory(dir)) {
                    log.info("创建用户根目录成功：{}", dirname);
                }
            }
            //关闭连接
            ftpClient.disconnect();
        } catch (Exception e) {
            log.info("创建用户根目录失败{}:", dirname);
            e.printStackTrace();
        }
    }

    //创建文件夹
    public static void createFolder(String foldername) {

        FTPClient ftpClient = null;
        try {
            //连接ftp
            ftpClient = getFtpClient();
            //创建文件夹
            ftpClient.makeDirectory(foldername);
            //关闭连接
            ftpClient.disconnect();
        } catch (Exception e) {
            log.info("创建文件夹失败{}:", foldername);
        }
    }

    /**
     * 进入用户的仓库
     *
     * @param dir 仓库id
     */
    public static boolean changeWorkingDirectory(String dir) {
        FTPClient ftpClient = null;
        try {
            //连接ftp
            ftpClient = getFtpClient();
            //目录编码，解决中文路径问题
            String dn = new String(dir.toString().getBytes("GBK"), "iso-8859-1");
            //切入用户仓库
            if (!ftpClient.changeWorkingDirectory(dn))
                return false;
        } catch (Exception e) {
            log.info("进入仓库失败：{}", dir);
        }
        return true;
    }


    /**
     * 通过浏览器形式下载文件
     */
    public Pair<Boolean, String> downloadFile(String fileStoreId, String ftpFilePath, String fileName, HttpServletResponse response) {
        InputStream input = null;
        OutputStream out = null;
        FTPClient ftpClient = null;
        try {
            //获取连接
            ftpClient = getFtpClient2();
            //重置
            response.reset();
            //此处你就根据你要下载的类型去设置即可,比如我下载.avi格式的文件，可以设置response.setContentType("video/avi");
            response.setContentType("application/download");
            // 设置文件ContentType类型，这样设置，会自动判断下载文件类型
            //response.setContentType("application/x-msdownload");
            //解决中文不能生成文件
            //response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName,"UTF-8"));
            //传输模式
            ftpClient.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
            // 设置以二进制流的方式传输
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            String path ="/" + fileStoreId + ftpFilePath;
            //进入目录
            ftpClient.changeWorkingDirectory(path);
            FTPFile[] files = ftpClient.listFiles();
            if (files.length < 1) {
                return Pair.of(false, "目录为空");
            }
            boolean fileExist = false;
            boolean downloadFlag = false;
            for (FTPFile ftpFile : files) {
                String ftpfileName = new String(ftpFile.getName().getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                if (fileName.equals(ftpfileName)) {
                    fileExist = true;
                    input = ftpClient.retrieveFileStream(new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));
                    out = response.getOutputStream();
                    int len;
                    byte[] bytes = new byte[1024];
                    while ((len = input.read(bytes)) != -1) {
                        out.write(bytes, 0, len);
                    }
                    out.flush();
                    downloadFlag = true;
                    break;
                }
            }
            if (!fileExist) {
                return Pair.of(false, "FTP服务器上文件不存在");
            }
            return Pair.of(downloadFlag, downloadFlag ? "下载成功" : "下载失败");

        } catch (IOException e) {
            e.printStackTrace();
            return Pair.of(false, "下载文件异常");
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (input != null) {
                    input.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 上传文件(有一点bug)
     */
    public static boolean fileupload(MultipartFile file, MyFileEntity entity, String newfileName) {
        boolean flag = false;
        FTPClient ftpClient = null;
        InputStream inputStream = null;
        try {
            //把文件转化为流
            //inputStream = new FileInputStream("F:\\实习\\12.txt");
            //inputStream = new BufferedInputStream(new FileInputStream(transferToFile(file)));
            inputStream = file.getInputStream();
            //连接ftp
            ftpClient = getFtpClient();
            //设置编码
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            //检查目录是否存在
            if (ftpClient.changeWorkingDirectory(String.valueOf(entity.getFileStoreId()))) {
                //获取文件类型
                String fileType = transformation(entity.getType());
                //切入用户仓库
                if (ftpClient.changeWorkingDirectory(fileType)) {
                    //工作模式被动
                    ftpClient.enterLocalPassiveMode();
                    //上传类型文件
                    if (ftpClient.storeFile(new String((newfileName).getBytes("GBK"), "iso-8859-1"), inputStream)) {
                        log.info("文件上传成功：{}", file.getOriginalFilename());
                        flag = true;
                    }
                }
            }
            //关闭流
            inputStream.close();
            //关闭连接
            ftpClient.disconnect();
        } catch (Exception e) {
            log.info("上传文件失败：{}", file.getName());
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 文件类型转换为文件
     */
    public static String transformation(Integer type) {
        String result = null;
        if (type == null)
            return result;
        //获取文件目录名
        List<String> list = Arrays.asList(ApplicaionUtil.files);
        if (type == 1) {
            result = list.get(1);
        } else if (type == 2) {
            result = list.get(2);
        } else if (type == 3) {
            result = list.get(3);
        } else if (type == 4) {
            result = list.get(4);
        } else if (type == 5) {
            result = list.get(5);
        }
        return result;
    }


    /**
     * MultipartFile 转换为 File 文件
     *
     * @param multipartFile
     * @return
     */
    public final static File transferToFile(MultipartFile multipartFile) {
        //选择用缓冲区来实现这个转换即使用java 创建的临时文件 使用 MultipartFile.transferto()方法 。
        File file = null;
        try {
            String originalFilename = multipartFile.getOriginalFilename();
            //获取文件后缀
            String prefix = originalFilename.substring(originalFilename.lastIndexOf("."));
            file = File.createTempFile(originalFilename, prefix);    //创建临时文件
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }


    /*// MultipartFile转换为InputStream
    public static InputStream multipartToInputStream(MultipartFile multipartFile) throws IOException {
        InputStream inputStream = null;
        File file = null;
        try {
            // 创建临时文件
            file = File.createTempFile("temp", null);
            // 把multipartFile写入临时文件
            multipartFile.transferTo(file);
            // 使用文件创建 inputStream 流
            inputStream = new FileInputStream(file);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 最后记得删除文件
            file.deleteOnExit();
            // 关闭流
            inputStream.close();
        }
        return inputStream;
    }
*/
}
