package com.lin.server.controller.sys;

import com.google.common.collect.Maps;
import com.lin.api.response.RESTfulResp;
import com.lin.api.utils.Constant;
import com.lin.model.entity.SysMenuEntity;
import com.lin.server.controller.common.AbstractController;
import com.lin.server.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController {

    private final SysMenuService sysMenuService;

    public SysMenuController(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    @RequestMapping("/list")
    public List<SysMenuEntity> list(){
        //使用SQL实现
        return sysMenuService.queryAll();
    }

    //获取树形层级列表数据
    @RequestMapping("/select")
    public Object info(){
        Map<String,Object> resMap = Maps.newHashMap();
        try {

           List<SysMenuEntity> menuList =  sysMenuService.queryNotButtonList();
           SysMenuEntity root = new SysMenuEntity();
           root.setMenuId((int) (Constant.TOP_MENU_ID));
           root.setName(Constant.TOP_MENU_NAME);
           root.setParentId(-1);
           menuList.add(root);
           resMap.put("menuList",menuList);

        }catch (Exception e){
            return RESTfulResp.internalServerError("获取数形层列表数据失败",e);
        }
        return RESTfulResp.ok(resMap);
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object save(@RequestBody @Validated SysMenuEntity entity){
        try {
            log.info("新增菜单~接收数据：",entity);
            String result = this.validateForm(entity);
            if (StringUtils.isNotBlank(result)){
                return RESTfulResp.internalServerError("",result);
            }
            sysMenuService.save(entity);
        }catch (Exception e){
            return RESTfulResp.internalServerError("保存菜单数据失败：",e.getMessage());
        }

        return RESTfulResp.ok();
    }
    //获取菜单详情
    @RequestMapping(value = "/info/{menuId}")
    public Object info(@PathVariable Long menuId){
        if (menuId == null || menuId <=0){
            return RESTfulResp.internalServerError("失败");
        }
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            resMap.put("menu",sysMenuService.getById(menuId));
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    //修改
    @RequestMapping(value = "/update",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object update(@RequestBody @Validated SysMenuEntity entity){
        try {

            log.info("修改菜单~接收数据",entity);
            String result = this.validateForm(entity);
            if (StringUtils.isNotBlank(result)){
                return RESTfulResp.internalServerError("失败",result);
            }
            sysMenuService.updateById(entity);
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok();
    }

    //删除
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    public Object delete(Long menuId){
        if (menuId == null || menuId <= 0){
            return RESTfulResp.internalServerError("失败");
        }
        try {
            log.info("删除菜单~接收数据",menuId);
            SysMenuEntity entity = sysMenuService.getById(menuId);
            if (entity == null){
                return RESTfulResp.internalServerError("失败");
            }
           List<SysMenuEntity> list =  sysMenuService.queryByParentId(entity.getMenuId());
            if (list != null && !list.isEmpty()){
                return RESTfulResp.internalServerError("该菜单下有子菜单，请先删除子菜单！");
            }
            sysMenuService.removeById(menuId);
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }

        return RESTfulResp.ok();
    }


    //验证参数是否正确
    private String validateForm(SysMenuEntity menu) {
        if (StringUtils.isBlank(menu.getName())) {
            return "菜单名称不能为空";
        }
        if (menu.getParentId() == null) {
            return "上级菜单不能为空";
        }

        //菜单
        if (menu.getType() == Constant.MenuType.MENU.getValue()) {
            if (StringUtils.isBlank(menu.getUrl())) {
                return "菜单链接url不能为空";
            }
        }

        //上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();

        if (menu.getParentId() != 0) {
            SysMenuEntity parentMenu = sysMenuService.getById(menu.getParentId());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if (menu.getType() == Constant.MenuType.CATALOG.getValue() || menu.getType() == Constant.MenuType.MENU.getValue()) {
            if (parentType != Constant.MenuType.CATALOG.getValue()) {
                return "上级菜单只能为目录类型";
            }
            return "";
        }

        //按钮
        if (menu.getType() == Constant.MenuType.BUTTON.getValue()) {
            if (parentType != Constant.MenuType.MENU.getValue()) {
                return "上级菜单只能为菜单类型";
            }
            return "";
        }

        return "";
    }

    @RequestMapping("/nav")
    public Object nav(){
        Map<String ,Object> resMap = new HashMap<>();
        try {
           List<SysMenuEntity> menuList =  sysMenuService.getUserMenuList(getUserId());
            resMap.put("menuList",menuList);
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok(resMap);
    }

}
