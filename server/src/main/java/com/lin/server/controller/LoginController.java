package com.lin.server.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.lin.api.response.RESTfulResp;
import com.lin.server.shiro.ShiroUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;

@Controller
public class LoginController {

    @Autowired
    private Producer producer;

    /**
     * 生成验证码方法二
     */
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response) throws Exception {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtil.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);

        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);

        System.out.println("验证码："+text);
    }

    /**
     * 用户登录
     * @param username
     * @param password
     * @param captcha
     * @return
     */
    @PostMapping("sys/login")
    @ResponseBody
    public Object login(String username,String password,String captcha){
        //校验验证码
        String kaptcha = ShiroUtil.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
        if (!kaptcha.equals(captcha)){
            return RESTfulResp.badRequest("验证码不正确！");
        }
        //提交按钮
        Subject subject = SecurityUtils.getSubject();
        try {
            if (!subject.isAuthenticated()){
                UsernamePasswordToken token = new UsernamePasswordToken(username, password);
                subject.login(token);
            }
        }catch (UnknownAccountException e){
            return RESTfulResp.badRequest(e.getMessage());
        }catch (IncorrectCredentialsException e){
            return RESTfulResp.badRequest("账号密码不匹配!");
        }catch (LockedAccountException e){
            return RESTfulResp.badRequest("账号已被锁定,请联系管理员!");
        } catch (AuthenticationException e) {
            return RESTfulResp.badRequest("账户验证失败!");
        }
        return RESTfulResp.ok();
    }

    @GetMapping("/loginOut")
    public String loginOut(){
        //销毁当前的shiro的用户session
        ShiroUtil.logout();
        return "redirect:/login.html";
    }
}
