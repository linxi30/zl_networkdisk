package com.lin.server.controller.sys;


import com.google.common.collect.Maps;
import com.lin.api.response.RESTfulResp;
import com.lin.api.utils.PageUtil;
import com.lin.api.utils.ValidatorUtil;
import com.lin.model.entity.SysRoleEntity;
import com.lin.server.controller.common.AbstractController;
import com.lin.server.service.SysRoleMenuService;
import com.lin.server.service.SysRoleService;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Mapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {


    private final SysRoleService sysRoleService;

    private final SysRoleMenuService sysRoleMenuService;

    public SysRoleController(SysRoleService sysRoleService, SysRoleMenuService sysRoleMenuService) {
        this.sysRoleService = sysRoleService;
        this.sysRoleMenuService = sysRoleMenuService;
    }

    @RequestMapping("list")
    //@RequiresPermissions("sys:role:list")
    public Object getList(@RequestParam Map<String,Object> paramMap){
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            log.info("角色模块~分页列表迷糊查询：",paramMap);
            PageUtil page = sysRoleService.queryPage(paramMap);
            resMap.put("page",page);
        }catch (Exception e){
            return RESTfulResp.badRequest("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    @RequestMapping("select")
    public Object select(){
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            log.info("角色列表~select");
            resMap.put("list",sysRoleService.list());
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    //添加
    @RequestMapping(value = "/save",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @RequiresPermissions("sys:role:save")
    public Object save(@RequestBody @Validated SysRoleEntity entity, BindingResult result){
        String res = ValidatorUtil.checkResult(result);
        if (StringUtils.isNotBlank(res)){
            return RESTfulResp.internalServerError(res);
        }
        try {
            log.info("新增角色~接收数据：",entity);
            sysRoleService.saveRole(entity);
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok();
    }

    //获取菜单详情
    @RequestMapping(value = "/info/{id}")
    public Object info(@PathVariable Integer id){
        if (id == null || id <= 0){
            return RESTfulResp.internalServerError("失败");
        }
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            SysRoleEntity entity = sysRoleService.getById(id);
            //获取角色对应的菜单列表
           List<Integer> menuIdList =  sysRoleMenuService.queryMenuIdList(id);
           entity.setMenuIdList(menuIdList);
           resMap.put("role",entity);
        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    //修改
    @RequestMapping(value = "/update",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @RequiresPermissions("sys:role:update")
    public Object update(@RequestBody @Validated SysRoleEntity entity,BindingResult result){
        String res = ValidatorUtil.checkResult(result);
        if (StringUtils.isNotBlank(res)){
            return RESTfulResp.internalServerError("失败");
        }
        try {
            log.info("修改角色~接收数据：{}",entity);
            sysRoleService.updateRole(entity);
        }catch (Exception e){
            return RESTfulResp.internalServerError(e.getMessage());
        }
        return RESTfulResp.ok();
    }


    //删除
    @RequestMapping(value = "/delete",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @RequiresPermissions("sys:role:delete")
    public Object delete(@RequestBody Integer[] ids){
        if (ids == null || ids.length <= 0){
            return RESTfulResp.internalServerError("失败");
        }
        try {
            log.info("删除角色~接收数据：{}",ids);
            sysRoleService.deleteBatch(ids);
        }catch (Exception e){
            return RESTfulResp.internalServerError(e.getMessage());
        }
        return RESTfulResp.ok();
    }
}
