package com.lin.server.controller.sys;


import com.google.common.collect.Maps;
import com.lin.api.response.RESTfulResp;
import com.lin.api.utils.Constant;
import com.lin.api.utils.PageUtil;
import com.lin.api.utils.ValidatorUtil;
import com.lin.model.entity.SysUserEntity;
import com.lin.server.annotation.LogAnnotation;
import com.lin.server.controller.common.AbstractController;
import com.lin.server.service.SysUserService;
import com.lin.server.shiro.ShiroUtil;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {

    @Autowired
    private SysUserService sysUserService;
    /**
     * 获取个人信息
     * @return
     */
    @RequestMapping("info")
    public Object getInfo(){
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            resMap.put("user",getUser());
        } catch (Exception e) {
            return RESTfulResp.badRequest("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    @RequestMapping("list")
    public Object getList(@RequestParam Map<String,Object> paramMap){
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            log.info("用户模块~分页列表迷糊查询：",paramMap);
            PageUtil page = sysUserService.queryPage(paramMap);
            resMap.put("page",page);
        }catch (Exception e){
            return RESTfulResp.badRequest("失败");
        }
        return RESTfulResp.ok(resMap);
    }



    @PostMapping("password")
    @LogAnnotation("修改登录密码")
    public Object EditPassword(String password,String newPassword){
        if (StringUtils.isBlank(password) || StringUtils.isBlank(newPassword)){
            return RESTfulResp.badRequest("密码不能为空!");
        }
        try {
            //先判断密码是否正确，再更新密码
            SysUserEntity entity = getUser();
            final String salt = entity.getSalt();
            String oldPsd = ShiroUtil.sha256(password, salt);
            if (!entity.getPassword().equals(oldPsd)){
                return RESTfulResp.badRequest("原密码不正确!");
            }
            String newPsd = ShiroUtil.sha256(newPassword, salt);
            //执行更新密码的逻辑
            log.info("~~~~~旧密码正确，开始更新新的密码~~~~~");
            sysUserService.updatePassword(entity.getUserId(),newPsd);
        }catch (Exception e){
            return RESTfulResp.internalServerError("修改密码失败！");
        }
        return RESTfulResp.ok("密码修改成功");
    }

    @RequestMapping("save")
    public Object save(@RequestBody @Validated SysUserEntity entity, BindingResult result){
        String res = ValidatorUtil.checkResult(result);
        if (StringUtils.isNotBlank(res)){
            return RESTfulResp.internalServerError(res);
        }
        try {
            log.info("新增用户~接收数据:{}",entity);
            sysUserService.saveUser(entity);

        }catch (Exception e){
            return RESTfulResp.badRequest("失败！");
        }
        return RESTfulResp.ok();
    }

    /**
     * 获取用户详情
     * @param userId
     * @return
     */
    @RequestMapping("/info/{userId}")
    public Object info(@PathVariable Integer userId){
        Map<String,Object> resMap = Maps.newHashMap();
        try {
            log.info("用户模块~获取详情：{}",userId);
            resMap.put("user",sysUserService.getInfo(userId));
        }catch (Exception e){
            return RESTfulResp.badRequest("失败！");
        }
        return RESTfulResp.ok(resMap);
    }

    @PostMapping(value = "/update")
    public Object update(@RequestBody SysUserEntity entity,BindingResult result){
        String res = ValidatorUtil.checkResult(result);
        if (StringUtils.isNotBlank(res)){
            return RESTfulResp.internalServerError(res);
        }
        try {
            log.info("修改用户~接收数据:{}",entity);
            sysUserService.updateUser(entity);
        }catch (Exception e){
            return RESTfulResp.internalServerError(e.getMessage());
        }
        return RESTfulResp.ok();
    }

    /**
     * 删除用户
     */
    @RequestMapping("/delete")
    public Object delete(@RequestBody Integer[] ids){
        if (ids == null || ids.length <= 0){
            return RESTfulResp.internalServerError("失败");
        }
        //超级管理员~admin不能删除，当前登录用户不能删除
       /* if (ArrayUtils.contains(ids,Constant.SUPER_ADMIN)){

        }*/
        if (Arrays.asList(ids).contains(Constant.SUPER_ADMIN)){
            return RESTfulResp.internalServerError("超级系统管理员不能被删除!");
        }
        if (ArrayUtils.contains(ids,getUserId())){
            return RESTfulResp.internalServerError("当前用户不能删除自己!");
        }
        try {
            sysUserService.deleteUser(ids);

        }catch (Exception e){
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok();
    }
}
