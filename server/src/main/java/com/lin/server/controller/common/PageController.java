package com.lin.server.controller.common;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {


    @RequestMapping("modules/{module}/{page}.html")
    public String page(@PathVariable String module, @PathVariable String page, Integer courseId, ModelMap modelMap){
        if (courseId!=null && courseId>=0){
            modelMap.put("courseId",courseId);
        }
        return "modules/"+module+"/"+page;
    }


    @RequestMapping(value = {"index.html","/"} )
    public String index(){
        return "index";
    }



    @RequestMapping("login.html")
    public String login(){
        if (SecurityUtils.getSubject().isAuthenticated()){
            return "redirect:index.html";
        }
        return "login";
    }
    @RequestMapping("register.html")
    public String register(){
        return "register";
    }
    @RequestMapping("main.html")
    public String main(){
        return "main";
    }

}
