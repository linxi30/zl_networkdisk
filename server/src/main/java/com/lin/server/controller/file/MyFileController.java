package com.lin.server.controller.file;

import com.google.common.collect.Maps;
import com.lin.api.response.RESTfulResp;
import com.lin.api.utils.PageUtil;
import com.lin.api.utils.ValidatorUtil;
import com.lin.model.entity.MyFileEntity;
import com.lin.model.entity.SysUserEntity;
import com.lin.server.controller.common.AbstractController;
import com.lin.server.service.MyFileService;
import com.lin.server.service.SysUserService;
import com.lin.server.utils.FtpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/file/my")
@Slf4j
public class MyFileController extends AbstractController {


    @Autowired
    private MyFileService myFileService;

    @Autowired
    private SysUserService sysUserService;

    /**
     *
     */
    @RequestMapping("/info/{myFileId}")
    public Object info(@PathVariable Integer myFileId) {
        if (myFileId == null || myFileId <= 0) {
            return RESTfulResp.internalServerError("失败");
        }
        Map<String, Object> resMap = Maps.newHashMap();
        try {
            MyFileEntity entity = myFileService.getById(myFileId);
            resMap.put("myfile", entity);
        } catch (Exception e) {
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    /**
     * 展示文件列表
     *
     * @param paramMap
     * @return
     */
    @RequestMapping("/list")
    public Object list(@RequestParam Map<String, Object> paramMap) {
        Map<String, Object> resMap = new HashMap<>();
        //获取登录用户仓库id
        SysUserEntity entity = sysUserService.getById(getUserId());
        try {
            log.info("全部文件~分页列表迷糊查询：", paramMap);
            PageUtil page = myFileService.queryPage(paramMap, entity.getFileStoreId());
            resMap.put("page", page);
        } catch (Exception e) {
            return RESTfulResp.internalServerError("失败");
        }
        return RESTfulResp.ok(resMap);
    }

    /**
     * 上传文件
     *
     * @param file
     * @param type
     * @return
     * @throws Exception
     */
    @PostMapping("/uploadfile")
    public Object uploadfile(@RequestParam("file") MultipartFile file, @RequestParam("type") Integer type) throws Exception {
        Map<String, Object> resMap = new HashMap<>();
        try {
            log.info("文件上传~获取文件名：", file.getOriginalFilename());
            myFileService.saveFile(file, type, getUser());
        } catch (Exception e) {
            return RESTfulResp.internalServerError("该文件名已经存在，不可重复！");
        }
        return RESTfulResp.ok();
    }

    @RequestMapping("/download")
    public void downloadfile(
            HttpServletResponse response,
            @RequestParam String myFilePath) {
        try {
            System.out.println("myFilePath:"+myFilePath);
            FtpUtil ftpUtil = new FtpUtil();
            ftpUtil.downloadFile("208800","/documents", "8d1daf53-inser",response);

        } catch (Exception e) {
            log.error("下载异常", e);
        }
    }
/*    @RequestMapping(value = "/download")
    public void downloadfile(
            HttpServletResponse response) {
        try {
            FtpUtil ftpUtil = new FtpUtil();
            ftpUtil.downloadFile("208800", "/documents", "8d1daf53-inser",response);
        } catch (Exception e) {
            log.error("下载异常", e);
        }
    }*/

}
