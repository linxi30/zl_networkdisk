package com.lin.server.shiro;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

/**
 * 推送给前端使用的shiro对象变量
 */
@Component
public class ShiroVariable {

    /**
     * 判断当前用户登录主体是否有指定的权限
     * @param permission
     * @return
     */
    public Boolean hasPermission(String permission){
        Subject subject = SecurityUtils.getSubject();
        return (subject != null && subject.isPermitted(permission)) ? true : false;
    }
}
