package com.lin.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.model.entity.FileStoreEntity;

public interface FileStoreService extends IService<FileStoreEntity> {

    void save(int userId,int fileStoreId);
}
