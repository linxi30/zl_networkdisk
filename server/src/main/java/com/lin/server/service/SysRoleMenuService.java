package com.lin.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.model.entity.SysRoleMenuEntity;

import java.util.List;

public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

    void saveOrUpdate(int roleId, List<Integer> menuIdList);

    List<Integer> queryMenuIdList(Integer roleId);

    void deleteBatch(List<Integer> roleIds);
}
