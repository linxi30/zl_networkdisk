package com.lin.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.model.entity.SysUserRoleEntity;
import com.lin.model.mapper.SysUserRoleMapper;

import java.util.List;

public interface SysUserRoleService extends IService<SysUserRoleEntity> {

    void saveOrUpdate(int userId, List<Integer> roleIds);

    List<Integer> queryRoleIdList(Integer userId);

    void deleteBatch(List<Integer> roleIds);
}
