package com.lin.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.model.entity.FileFolderEntity;

public interface FileFolderService extends IService<FileFolderEntity> {
}
