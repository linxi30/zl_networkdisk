package com.lin.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.api.utils.PageUtil;
import com.lin.model.entity.SysRoleEntity;

import java.util.Map;

public interface SysRoleService extends IService<SysRoleEntity> {

    PageUtil queryPage(Map<String, Object> paramMap);

    void saveRole(SysRoleEntity entity);

    void updateRole(SysRoleEntity entity);

    void deleteBatch(Integer[] ids);
}
