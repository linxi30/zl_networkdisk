package com.lin.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.api.utils.Constant;
import com.lin.model.entity.FileStoreEntity;
import com.lin.model.mapper.FileStoreMapper;
import com.lin.server.service.FileStoreService;
import org.springframework.stereotype.Service;

@Service
public class FileStoreServiceImpl extends ServiceImpl<FileStoreMapper, FileStoreEntity> implements FileStoreService {


    //维护好用户~仓库的关联关系
    @Override
    public void save(int userId, int fileStoreId) {
        //用户添加时自动添加仓库
        FileStoreEntity fileStoreEntity = new FileStoreEntity(
                fileStoreId,userId, Constant.DefaultMinCapacity,Constant.DefaultMaxCapacity,Constant.PermissionType.allowUandD.getValue());
        this.save(fileStoreEntity);
    }

}
