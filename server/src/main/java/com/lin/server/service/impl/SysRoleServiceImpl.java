package com.lin.server.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.api.utils.PageUtil;
import com.lin.api.utils.QueryUtil;
import com.lin.model.entity.SysRoleEntity;
import com.lin.model.mapper.SysRoleMapper;
import com.lin.server.service.SysRoleMenuService;
import com.lin.server.service.SysRoleService;
import com.lin.server.service.SysUserRoleService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService {
    private static final Logger log= LoggerFactory.getLogger(SysRoleServiceImpl.class);

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Override
    public PageUtil queryPage(Map<String, Object> paramMap) {

        String search = (paramMap.get("search") != null ? (String) paramMap.get("search") :"");
        IPage<SysRoleEntity> iPage = new QueryUtil<SysRoleEntity>().getQueryPage(paramMap);
        QueryWrapper<SysRoleEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(search),"role_name",search.trim());
        IPage<SysRoleEntity> resPage = this.page(iPage,queryWrapper);

        return new PageUtil(resPage);
    }

    //新增
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveRole(SysRoleEntity entity) {
        entity.setCreateTime(DateTime.now().toDate());
        this.save(entity);
        //插角色~菜单关联关系
        sysRoleMenuService.saveOrUpdate(entity.getRoleId(),entity.getMenuIdList());
    }

    //更新修改
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateRole(SysRoleEntity entity) {
        this.updateById(entity);
        //更新角色~菜单关联关系信息
        sysRoleMenuService.saveOrUpdate(entity.getRoleId(),entity.getMenuIdList());
    }

    //批量删除
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteBatch(Integer[] ids) {
        List<Integer> roleIds = Arrays.asList(ids);
        this.removeByIds(roleIds);

        //删除角色~菜单关联数据
        sysRoleMenuService.deleteBatch(roleIds);

        //删除角色~用户关联数据
        sysUserRoleService.deleteBatch(roleIds);
    }
}
