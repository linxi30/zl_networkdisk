package com.lin.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.api.utils.Constant;
import com.lin.api.utils.PageUtil;
import com.lin.api.utils.QueryUtil;
import com.lin.model.entity.FileStoreEntity;
import com.lin.model.entity.SysUserEntity;
import com.lin.model.entity.SysUserRoleEntity;
import com.lin.model.mapper.SysUserMapper;
import com.lin.server.service.FileStoreService;
import com.lin.server.service.SysRoleService;
import com.lin.server.service.SysUserRoleService;
import com.lin.server.service.SysUserService;
import com.lin.server.shiro.ShiroUtil;
import com.lin.server.utils.FileUtil;
import com.lin.server.utils.FtpUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {




    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private FileStoreService fileStoreService;

    @Override
    public boolean updatePassword(int userId, String newPsd) {
       return baseMapper.updatePassword(userId,newPsd);
    }

    //用户列表的分页查询
    @Override
    public PageUtil queryPage(Map<String, Object> paramMap) {
        String search = (paramMap.get("search") != null) ? (String)paramMap.get("search"):"";
        IPage<SysUserEntity> iPage = new QueryUtil<SysUserEntity>().getQueryPage(paramMap);
        QueryWrapper<SysUserEntity> wrapper = new QueryWrapper<>();
        wrapper.like(StringUtils.isNotBlank(search),"username",search.trim())
                .or(StringUtils.isNotBlank(search.trim()))
                .like(StringUtils.isNotBlank(search),"name",search.trim());
        IPage<SysUserEntity> resPage = this.page(iPage,wrapper);
        return new PageUtil(resPage);
    }

    //保存用户
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveUser(SysUserEntity entity) {

        if (this.getOne(new QueryWrapper<SysUserEntity>().eq("username",entity.getUsername())) != null){
            throw new RuntimeException("用户名已存在!");
        }
        entity.setCreateTime(new Date());

        //加密密码字符串
        String salt = RandomStringUtils.randomAlphabetic(20);
        String password = ShiroUtil.sha256(entity.getPassword(), salt);
        entity.setPassword(password);
        entity.setSalt(salt);

        //文件仓库ID
        int fileStoreId = (int)((Math.random()*9+1)*100000);
        entity.setFileStoreId(fileStoreId);
        //添加用户
        this.save(entity);
       /* //ftp端建立联系
        FtpUtil.createDir(String.valueOf(fileStoreId));*/
        //localhost端建立联系
        FileUtil.createDir(String.valueOf(fileStoreId));
        //维护好用户~仓库的关联关系
        fileStoreService.save(entity.getUserId(),fileStoreId);
        //维护好用户~角色的关联关系
        sysUserRoleService.saveOrUpdate(entity.getUserId(),entity.getRoleIdList());
    }

    //获取用户详情~包括其分配的角色
    @Override
    public SysUserEntity getInfo(Integer userId) {
        SysUserEntity entity = this.getById(userId);
        //获取用户分配的角色关联关系
        List<Integer> roleids = sysUserRoleService.queryRoleIdList(userId);
        entity.setRoleIdList(roleids);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(SysUserEntity entity) {
        SysUserEntity oldUser = this.getById(entity.getUserId());
        if (oldUser == null){
            return;
        }
        if (!oldUser.getUsername().equals(entity.getUsername())) {
            if (this.getOne(new QueryWrapper<SysUserEntity>().eq("username",entity.getUsername())) != null) {
                throw new RuntimeException("用户名已存在");
            }
        }
        if (StringUtils.isNotBlank(entity.getPassword())){
            String newPassword = ShiroUtil.sha256(entity.getPassword(), oldUser.getSalt());
            entity.setPassword(newPassword);
        }

        this.updateById(entity);
        //维护好用户~角色的关联关系
        sysUserRoleService.saveOrUpdate(entity.getUserId(),entity.getRoleIdList());

    }

    //删除用户~除了删除用户本身的信息之外，还需要删除用户~角色 关联关系信息
    @Override
    public void deleteUser(Integer[] ids) {

        if (ids != null && ids.length > 0){
            List<Integer> userIds = Arrays.asList(ids);
            this.removeByIds(userIds);
         /*   for (Long userId : userIds) {
                sysUserRoleService.remove(new QueryWrapper<SysUserRoleEntity>().eq("user_id",userId));
                sysUserPostService.remove(new QueryWrapper<SysUserPostEntity>().eq("user_id",userId));
            }*/
            //java8的写法
            userIds.stream().forEach(userId -> sysUserRoleService.remove(new QueryWrapper<SysUserRoleEntity>().eq("user_id",userId)));
        }
    }
}
