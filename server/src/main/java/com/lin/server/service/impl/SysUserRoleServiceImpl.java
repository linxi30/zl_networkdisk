package com.lin.server.service.impl;


import com.baomidou.mybatisplus.core.conditions.interfaces.Join;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.lin.api.utils.CommonUtil;
import com.lin.model.entity.SysUserRoleEntity;
import com.lin.model.mapper.SysUserRoleMapper;
import com.lin.server.service.SysUserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper,SysUserRoleEntity> implements SysUserRoleService {


    //维护用户~角色的关联关系
    @Transactional(rollbackFor =  Exception.class)
    @Override
    public void saveOrUpdate(int userId, List<Integer> roleIds) {
        //需要清除旧的数据，再插入新的关联关系
        this.remove(new QueryWrapper<SysUserRoleEntity>().eq("user_id",userId));
        if (roleIds!=null && !roleIds.isEmpty()){
            SysUserRoleEntity entity;
            for (Integer rId : roleIds) {
                entity = new SysUserRoleEntity();
                entity.setRoleId(rId.intValue());
                entity.setUserId(userId);
                this.save(entity);
            }
        }
    }

    //获取分配给用户的角色的id列表
    @Override
    public List<Integer> queryRoleIdList(Integer userId) {
        return baseMapper.queryRoleIdList(userId);
    }


    @Override
    public void deleteBatch(List<Integer> roleIds) {
        if (roleIds != null || roleIds.isEmpty()){
            String delIds = Joiner.on(",").join(roleIds);
            baseMapper.deleteBatch(CommonUtil.concatStrToInt(delIds,","));
        }
    }
}
