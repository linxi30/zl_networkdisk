package com.lin.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.api.utils.PageUtil;
import com.lin.api.utils.QueryUtil;
import com.lin.model.entity.MyFileEntity;
import com.lin.model.entity.SysUserEntity;
import com.lin.model.mapper.MyFileMapper;
import com.lin.model.mapper.SysUserMapper;
import com.lin.server.service.MyFileService;
import com.lin.server.utils.ApplicaionUtil;
import com.lin.server.utils.FileUtil;
import com.lin.server.utils.FtpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@Service
@Slf4j
public class MyFileServiceImpl extends ServiceImpl<MyFileMapper, MyFileEntity> implements MyFileService {
    @Autowired
    private SysUserMapper sysUserMapper;

    //获取全部文件列表
    @Override
    public PageUtil queryPage(Map<String, Object> paramMap, Integer fileStoreId) {
        String search = (paramMap.get("search")!=null)?(String)paramMap.get("search"):"";
        IPage<MyFileEntity> iPage = new QueryUtil<MyFileEntity>().getQueryPage(paramMap);
        QueryWrapper<MyFileEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(String.valueOf(fileStoreId)),"file_store_id",fileStoreId)
                .like(StringUtils.isNotBlank(search),"my_file_name",search.trim());
        IPage<MyFileEntity> resPage = this.page(iPage, wrapper);
        return new PageUtil(resPage);
    }

    //保存文件
    @Override
    public boolean saveFile(MultipartFile file, Integer type, SysUserEntity userEntity) {
        //获取文件目录名
        List<String> list = Arrays.asList(ApplicaionUtil.files);
        MyFileEntity entity = new MyFileEntity();
        if (type == 1){
            entity.setMyFilePath("\\"+list.get(1));
        }else if (type == 2){
            entity.setMyFilePath("\\"+list.get(2));
        }else if (type == 3){
            entity.setMyFilePath("\\"+list.get(3));
        }else if (type == 4){
            entity.setMyFilePath("\\"+list.get(4));
        }else if (type == 5){
            entity.setMyFilePath("\\"+list.get(5));
        }
        entity.setFileStoreId(userEntity.getFileStoreId());
        String om = file.getOriginalFilename();
        //判断文件是否已经存在
        if(this.getOne(new QueryWrapper<MyFileEntity>().eq("my_file_name",om)) != null){
            throw new RuntimeException("该文件名已经存在！");
        }
        //String newfileName = UUID.randomUUID().toString().substring(0,8)+"-"+om.substring(0,om.lastIndexOf("."));
        entity.setMyFileName(om);
        entity.setPostfix(om.substring(om.indexOf(".")+1,om.length()));
        entity.setSize(file.getSize()*1024);
        entity.setUploadTime(new Date());
        entity.setType(type);
        entity.setIsRecovery(0);
        //数据库保存数据
        this.save(entity);
        //并上传至ftp服务器
    /*    if(FtpUtil.fileupload(file,entity,newfileName)){
            flag = true;
        }*/
        //上传至本地
        return FileUtil.fileupload(file,entity);
    }



}
