package com.lin.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.model.entity.FileFolderEntity;
import com.lin.model.mapper.FileFolderMapper;
import com.lin.server.service.FileFolderService;
import org.springframework.stereotype.Service;

@Service
public class FileFolderServiceImpl extends ServiceImpl<FileFolderMapper, FileFolderEntity> implements FileFolderService {
}
