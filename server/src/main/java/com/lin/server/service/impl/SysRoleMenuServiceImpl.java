package com.lin.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.lin.api.utils.CommonUtil;
import com.lin.model.entity.SysRoleMenuEntity;
import com.lin.model.mapper.SysRoleMenuMapper;
import com.lin.server.service.SysRoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.relation.Role;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenuEntity> implements SysRoleMenuService {

    //维护角色菜单关联信息
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(int roleId, List<Integer> menuIdList) {

        //需要先清除旧的角色的关联关系，再插入新的关联关系
        deleteBatch(Arrays.asList(roleId));

        SysRoleMenuEntity entity;
        if (menuIdList != null || !menuIdList.isEmpty()){
            for (Integer mId : menuIdList) {
                entity = new SysRoleMenuEntity();
                entity.setMenuId(mId);
                entity.setRoleId(roleId);
                this.save(entity);
            }
        }
    }

    //获取角色id批量删除
    @Override
    public List<Integer> queryMenuIdList(Integer roleId) {
        return baseMapper.queryMenuIdList(roleId);
    }

    //根据角色id批量删除
    public void deleteBatch(List<Integer> roleIds) {
        if (roleIds != null && !roleIds.isEmpty()){
            String delIds= Joiner.on(",").join(roleIds);
            baseMapper.deleteBatch(CommonUtil.concatStrToInt(delIds,","));
        }
    }


}
