package com.lin.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.lin.api.utils.Constant;
import com.lin.model.entity.SysMenuEntity;
import com.lin.model.mapper.SysMenuMapper;
import com.lin.model.mapper.SysUserMapper;
import com.lin.server.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenuEntity> implements SysMenuService {

    @Autowired
    private SysUserMapper sysUserMapper;

    //获取不包含菜单的树形层级列表数据
    @Override
    public List<SysMenuEntity> queryAll() {

        return baseMapper.queryList();
    }
    //获取不包含按钮的菜单列表
    @Override
    public List<SysMenuEntity> queryNotButtonList() {
        return baseMapper.queryNotButtonList();
    }

    //根据父级菜单id查询其下的子菜单列表
    @Override
    public List<SysMenuEntity> queryByParentId(Integer menuId) {
        return baseMapper.queryListParentId(menuId);
    }

    //获取首页左边导航菜单栏
    @Override
    public List<SysMenuEntity> getUserMenuList(Integer currUserId) {

        List<SysMenuEntity> list = Lists.newLinkedList();
        if (Long.valueOf(currUserId) == Constant.SUPER_ADMIN){
            list = getAllMenuList(null);
        }else {
            //非超级管理员的情况~根据分配给用户角色~菜单的关联信息的获取
            List<Integer> menuIdList = sysUserMapper.queryAllMenuId(currUserId);
            list =  getAllMenuList(menuIdList);
        }
        return list;
    }

    //获取所以菜单列表
    private List<SysMenuEntity> getAllMenuList(List<Integer> menuIdList){
        List<SysMenuEntity> menuList = queryListByParentId(0, menuIdList);


        //递归获取一级菜单下的子菜单
        getMenuTrees(menuList,menuIdList);
        return menuList;
    }

    /**
     * 根据父id查询子菜单列表 ~找出一级菜单列表（找出类型为 目录 的菜单）
     * @param parentId
     * @return
     */
    private List<SysMenuEntity> queryListByParentId(Integer parentId,List<Integer> menuIdList){
        List<SysMenuEntity> menuList = baseMapper.queryListParentId(parentId);

        if (menuIdList == null || menuIdList.isEmpty()){
            return menuList;
        }
        //在所有的一级菜单列表中找出存在于用户分配的菜单列表中
        List<SysMenuEntity> userMenuList = Lists.newLinkedList();
        for (SysMenuEntity entity: menuList){
            if (menuIdList.contains(entity.getMenuId())){
                userMenuList.add(entity);
            }
        }
        return userMenuList;
    }

    /**
     * 递归思想
     * @param menuList
     * @param menuIdList
     * @return
     */
    private List<SysMenuEntity> getMenuTrees(List<SysMenuEntity> menuList,List<Integer> menuIdList){
        List<SysMenuEntity> subMenuList = Lists.newLinkedList();
        List<SysMenuEntity> tempList;
        for (SysMenuEntity entity: menuList){
            //当前菜单类型为目录（一级菜单）：当前菜单类型不为目录（当前菜单的子菜单为空）
           /* if (entity.getType() == Constant.MenuType.CATALOG.getValue()){
                tempList=queryListByParentId(entity.getMenuId(),menuIdList);
                entity.setList(getMenuTrees(tempList,menuIdList));

            }*/
            tempList = queryListByParentId(entity.getMenuId(),menuIdList);
            if (tempList!=null && !tempList.isEmpty()){
                entity.setList(getMenuTrees(tempList,menuIdList));
            }

            subMenuList.add(entity);
        }
        return subMenuList;
    }
}
