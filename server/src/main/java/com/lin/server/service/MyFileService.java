package com.lin.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.api.utils.PageUtil;
import com.lin.model.entity.MyFileEntity;
import com.lin.model.entity.SysUserEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface MyFileService extends IService<MyFileEntity> {

    PageUtil queryPage(Map<String, Object> paramMap, Integer fileStoreId);

    boolean saveFile(MultipartFile file, Integer type, SysUserEntity entity);

}
