package com.lin.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.api.utils.PageUtil;
import com.lin.model.entity.SysUserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;


public interface SysUserService extends IService<SysUserEntity> {


    boolean updatePassword(@Param("userId") int userId, @Param("newPsd") String newPsd);

    PageUtil queryPage(Map<String, Object> paramMap);

    void saveUser(SysUserEntity entity);

    SysUserEntity getInfo(Integer userId);

    void updateUser(SysUserEntity entity);

    void deleteUser(Integer[] ids);
}
