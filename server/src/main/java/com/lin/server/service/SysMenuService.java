package com.lin.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.model.entity.SysMenuEntity;

import java.util.List;

public interface SysMenuService extends IService<SysMenuEntity>{
    //获取所有菜单
    List<SysMenuEntity> queryAll();
    //获取不包含按钮的菜单列表
    List<SysMenuEntity> queryNotButtonList();
    //根据id查询父
    List<SysMenuEntity> queryByParentId(Integer menuId);

    List<SysMenuEntity> getUserMenuList(Integer userId);
}
