import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lin.model.entity.SysUserEntity;
import com.lin.model.mapper.SysUserMapper;
import com.lin.server.service.SysUserService;
import com.lin.server.utils.QRCodeUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.sound.midi.Soundbank;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@SpringBootTest
public class QRCodeUtilTest {

    public static void main(String[] args) throws IOException {
        Map<String,Object> resMap = new HashMap<>();
        String url = "http://114.132.50.104:8080/";
        BufferedImage image = QRCodeUtil.createImage("utf-8",url,300,200);
        QRCodeUtil.addUpFont(image,"你是笨蛋二维码");

        File file = new File("F:\\毕业设计\\我的网盘毕业项目\\images");
        if (!file.isDirectory()){
            file.mkdirs();
        }
        UUID uuid = UUID.randomUUID();
        File resultFile = new File(String.valueOf(file)+"\\"+(uuid.toString().replace("-","").substring(0,12))+".jpg");
        ImageIO.write(image,"JPG",resultFile);
    }

}
