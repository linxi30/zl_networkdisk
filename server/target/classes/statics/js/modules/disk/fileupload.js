
var ztree;

var vm = new Vue({
    el:'#pmpapp',
    data:{
        myfile:{
            type:null,
            myFileName:null,
        },
        upath:null,
    },
    methods: {
        fileupload: function(){
            if(vm.validator()){
                return;
            }
          vm.fileupload1();
        },

        fileupload1: function () {
            var forData = new FormData();
            forData.append('file',this.upath);
            forData.append('type',this.myfile.type);
            var url = "file/my/uploadfile";
            $.ajax({
                type: "POST",
                url:  baseURL + url,
                data: forData,
                contentType : false,
                processData : false,
                success: function(r){
                    if(r.code == 200){
                        alert('文件传输成功', function(){
                            vm.reload();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        getFile(event){
            this.upath = event.target.files[0];
        },
        reload: function () {
           vm.upath = '';
           vm.myfile.type = null;
        },
        //文件判断
        validator: function () {
            if(isBlank(vm.upath)){
                alert("文件未选择！");
                return true;
            }
            if(isBlank(vm.myfile.type)){
                alert("请选择文件类型");
                return true;
            }
        }

    }
});