$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'file/my/list',
        datatype: "json",
        colModel: [
            {label: '文件id', name: 'myFileId', index: "my_file_id", width: 45, key: true},
            {label: '文件名', name: 'myFileName', index: "my_file_name", width: 45},
            {label: '文件存储路径', name: 'myFilePath', index: "my_file_path", width: 100},
            {label: '文件大小kb', name: 'size', index: "size", width: 75},
            {label: '文件后缀', name: 'postfix', index: "postfix", width: 80},
            {label: '文件上传时间', name: 'uploadTime', index: "upload_time", width: 80},
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [5, 10, 20, 50, 100],
        rownumbers: true,
        rownumWidth: 25,
        autowidth: true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "data.page.list",
            page: "data.page.currPage",
            total: "data.page.totalPage",
            records: "data.page.totalCount"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });
});

var vm = new Vue({
    el: '#pmpapp',
    data: {
        q: {
            myFileName: null
        },
        showList: true,
        title: null,

        myfile: {
            myFileName: null,
            myFilePath: null,
        }
    },
    methods: {
        query: function () {
            vm.reload();
        },

        reload: function () {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'search': vm.q.myFileName},
                page: page
            }).trigger("reloadGrid");
        },

        download: function () {
            var myfileId = getSelectedRow();
            if (myfileId == null) {
                return;
            }

            vm.showList = false;
            vm.title = "文件详情";
            vm.getInfo(myfileId);
        },
        generate: function () {

        },
        del: function () {

        },
        //根据userId获取用户信息
        getInfo: function (myfileId) {
            $.get(baseURL + "file/my/info/" + myfileId, function (r) {
                vm.myfile = r.data.myfile;
            });
        },
    }
});